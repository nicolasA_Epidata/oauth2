package com.oauth.controlador;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/nombre")
public class Nombre {

    @GetMapping("/nico")
    public String obtenerNico() {
        return "nico";
    }
    @GetMapping("/jose")
    public String obtenerJose(){
        return "jose";
    }
}
